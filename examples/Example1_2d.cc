
#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <limits>
#include <utility>
#include <memory>
#include <algorithm>
#include <cassert>
#include <string>
#include <complex>
#include <set>
#include <random>
#include <sstream>
#include <chrono>

#include "YgorClustering.hpp"

int main(){

    typedef ClusteringDatum<2, double, 1, float, uint16_t> CDat_t;

    constexpr size_t MaxElementsInANode = 6; // 16, 32, 128, 256, ... ?
    typedef boost::geometry::index::rstar<MaxElementsInANode> RTreeParameter_t;
    typedef boost::geometry::index::rtree<CDat_t,RTreeParameter_t> RTree_t;
    typedef boost::geometry::model::box<CDat_t> Box_t;

    unsigned int nloops = 500;
    unsigned int min_duration = 100000;
    unsigned int max_duration = 0;
    unsigned int mean_duration = 0;

    for (size_t n=0; n<nloops; n++)
    {
        RTree_t rtree;

        //Stuff some more points in.
        size_t FixedSeed = std::chrono::milliseconds(std::chrono::seconds(std::time(NULL))).count();
        std::mt19937 re(FixedSeed);
        std::uniform_real_distribution<> rd(-100.0, 100.0);
        for(size_t i = 0; i < 250; ++i) rtree.insert(CDat_t( { rd(re), rd(re) }, { rd(re) }));

        std::uniform_real_distribution<> rd_2(-55.0, 5.0);
        for(size_t i = 0; i < 125; ++i) rtree.insert(CDat_t( { rd_2(re), rd_2(re) }, { rd_2(re) }));

        std::uniform_real_distribution<> rd_3(-5.0, 55.0);
        for(size_t i = 0; i < 125; ++i) rtree.insert(CDat_t( { rd_3(re), rd_3(re) }, { rd_3(re) }));

        //const size_t MinPts = CDat_t::SpatialDimensionCount_*2;
        //const SpatialQueryTechnique UsersSpatialQueryTechnique = SpatialQueryTechnique::UseWithin;
        const double Eps = 9.0;

        auto start = std::chrono::steady_clock::now();
        DBSCAN<RTree_t,CDat_t>(rtree,Eps);
        auto end = std::chrono::steady_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        mean_duration += duration;

        if (duration < min_duration)
        {
            min_duration = duration;
        }

        if (duration > max_duration)
        {
            max_duration = duration;
        }
    }

    mean_duration = mean_duration/nloops;

    std::cout << "Iterations: " << nloops
        << " | Mean: " << mean_duration
        << " ms | Max: " << max_duration
        << " ms | Min: " << min_duration << " ms" << std::endl;

    //Print out the points with cluster info.
    // if(true){
    //     constexpr auto RTreeSpatialQueryGetAll = [](const CDat_t &) -> bool { return true; };
    //     RTree_t::const_query_iterator it;
    //     it = rtree.qbegin(boost::geometry::index::satisfies( RTreeSpatialQueryGetAll ));
    //     for( ; it != rtree.qend(); ++it){
    //         std::cout << "  Point: " << boost::geometry::wkt(*it)
    //                   << "\t\t Attribute[0]: " << it->Attributes[0]
    //                   << "\t\t ClusterID: " << it->CID.ToText()
    //                   << std::endl;
    //     }
    // }

    //Stream out an svg diagram where colours denote clusters.
    // if(true){
    //     constexpr auto RTreeSpatialQueryGetAll = [](const CDat_t &) -> bool { return true; };
    //     std::ofstream svg("Visualized.svg");
    //     boost::geometry::svg_mapper<CDat_t> mapper(svg, 1280, 1024);

    //     //Add the items to the map. The virtual bounds will be computed to accomodate.
    //     // Also keep a record of the distinct clusters encountered.
    //     std::set<typename CDat_t::ClusterIDType_> RawCIDs;

    //     RTree_t::const_query_iterator it;
    //     it = rtree.qbegin(boost::geometry::index::satisfies( RTreeSpatialQueryGetAll ));
    //     for( ; it != rtree.qend(); ++it){
    //         mapper.add(*it);
    //         RawCIDs.insert( it->CID.Raw );
    //     }
    //     std::cout << RawCIDs.size() << " distinct ClusterIDs encountered." << std::endl;

    //     //Create a mapping between the ClusterIDs and a pseudo-random RGB colour.
    //     size_t ColourSeed = 9137;
    //     std::mt19937 re(ColourSeed);
    //     std::uniform_real_distribution<> rdC(0.0, 1.0);
    //     std::uniform_int_distribution<> rdA( 50, 210);
    //     std::uniform_int_distribution<> rdB( 20, 125);
    //     std::map<typename CDat_t::ClusterIDType_, std::string> ColoursA, ColoursB;
    //     for(const auto RawCID : RawCIDs){
    //         ColoursA[RawCID] = std::to_string((rdC(re) > 0.33) ? rdA(re) : 230) + std::string(",")
    //                          + std::to_string((rdC(re) > 0.33) ? rdA(re) : 230) + std::string(",")
    //                          + std::to_string((rdC(re) > 0.33) ? rdA(re) : 230);
    //         ColoursB[RawCID] = std::to_string((rdC(re) > 0.33) ? rdB(re) :  10) + std::string(",")
    //                          + std::to_string((rdC(re) > 0.33) ? rdB(re) :  10) + std::string(",")
    //                          + std::to_string((rdC(re) > 0.33) ? rdB(re) :  10);
    //     }

    //     //Actually draw the items.
    //     it = rtree.qbegin(boost::geometry::index::satisfies( RTreeSpatialQueryGetAll ));
    //     for( ; it != rtree.qend(); ++it){
    //         std::stringstream ss;
    //         ss << "fill-opacity:0.80; "
    //            << "fill:rgb(" << ColoursA[it->CID.Raw] << "); "
    //            << "stroke-opacity:0.90; "
    //            << "stroke:rgb(" << ColoursB[it->CID.Raw] << "); "
    //            << "stroke-width:1";
    //         //mapper.map(*it, "fill-opacity:0.75; fill:rgb(75,100,0); stroke:rgb(30,40,0); stroke-width:2", 5);
    //         mapper.map(*it, ss.str(), 6);
    //     }
    // }

    return 0;
}

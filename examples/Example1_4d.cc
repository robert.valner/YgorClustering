
#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <limits>
#include <utility>
#include <memory>
#include <algorithm>
#include <cassert>
#include <string>
#include <complex>
#include <set>
#include <random>
#include <sstream>
#include <chrono>

#include "YgorClustering.hpp"

int main(){

    typedef ClusteringDatum<4, double, 1, float, uint16_t> CDat_t;

    constexpr size_t MaxElementsInANode = 6; // 16, 32, 128, 256, ... ?
    typedef boost::geometry::index::rstar<MaxElementsInANode> RTreeParameter_t;
    typedef boost::geometry::index::rtree<CDat_t,RTreeParameter_t> RTree_t;
    typedef boost::geometry::model::box<CDat_t> Box_t;

    unsigned int nloops = 500;
    unsigned int min_duration = 100000;
    unsigned int max_duration = 0;
    unsigned int mean_duration = 0;

    for (size_t n=0; n<nloops; n++)
    {
        RTree_t rtree;

        //Stuff some more points in.
        size_t FixedSeed = std::chrono::milliseconds(std::chrono::seconds(std::time(NULL))).count();
        std::mt19937 re(FixedSeed);
        std::uniform_real_distribution<> rd(-100.0, 100.0);
        for(size_t i = 0; i < 250; ++i) rtree.insert(CDat_t( { rd(re), rd(re), rd(re), rd(re)  }, { rd(re) }));

        std::uniform_real_distribution<> rd_2(-55.0, 5.0);
        for(size_t i = 0; i < 125; ++i) rtree.insert(CDat_t( { rd_2(re), rd_2(re), rd_2(re), rd_2(re) }, { rd_2(re) }));

        std::uniform_real_distribution<> rd_3(-5.0, 55.0);
        for(size_t i = 0; i < 125; ++i) rtree.insert(CDat_t( { rd_3(re), rd_3(re), rd_3(re), rd_3(re) }, { rd_3(re) }));

        //const size_t MinPts = CDat_t::SpatialDimensionCount_*2;
        //const SpatialQueryTechnique UsersSpatialQueryTechnique = SpatialQueryTechnique::UseWithin;
        const double Eps = 9.0;

        auto start = std::chrono::steady_clock::now();
        DBSCAN<RTree_t,CDat_t>(rtree,Eps);
        auto end = std::chrono::steady_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        mean_duration += duration;

        if (duration < min_duration)
        {
            min_duration = duration;
        }

        if (duration > max_duration)
        {
            max_duration = duration;
        }
    }

    mean_duration = mean_duration/nloops;

    std::cout << "Iterations: " << nloops
        << " | Mean: " << mean_duration
        << " ms | Max: " << max_duration
        << " ms | Min: " << min_duration << " ms" << std::endl;

    return 0;
}
